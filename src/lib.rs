#![cfg(target_os = "linux")]
#![doc = include_str!("../README.md")]
#![doc(html_root_url = "https://docs.rs/sensors-sys/0.2.17")]
#![warn(unsafe_op_in_unsafe_fn)]

#[cfg(test)]
mod tests;

include!(concat!(env!("OUT_DIR"), "/sensors-sys.rs"));
