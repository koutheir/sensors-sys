#[test]
fn sensors_strerror() {
    let r = unsafe { super::sensors_strerror(super::SENSORS_ERR_IO) };
    assert!(!r.is_null());
}
